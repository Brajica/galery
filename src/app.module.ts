import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {configService} from './config/config.service';
import {PhotoModule} from './modules/photo/photo.module';
import {AlbumModule} from './modules/album/album.module';

@Module({
    imports: [
        TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
        PhotoModule,
        AlbumModule,
    ],
    controllers: [],
    providers: [AppService],
})
export class AppModule {
}
