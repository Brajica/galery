import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import {Album} from "../model/album.entity";
import {OuputAlbumDto} from "./ouputAlbum.dto";

export class AlbumDto {
    @ApiModelProperty({required: true})
    name: string;

    @ApiModelProperty({required: true})
    description: string;



    static OutputtoEntity(album: OuputAlbumDto) {
        const it = new Album();
        it.name = album.name;
        it.description = album.description;
        it.id = album.id.toString();
        return it;
    }
}
