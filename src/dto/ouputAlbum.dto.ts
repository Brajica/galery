import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import {Photo} from "../model/photo.entity";
import {PhotoDTO} from "./photo.dto";
import {Cloudinary} from "../utils/cloudinary/cloudinary.service";
import {AlbumDto} from "./almbum.dto";
import {Album} from "../model/album.entity";

export class OuputAlbumDto {
    @ApiModelProperty({required: false})
    id: number;

    @ApiModelProperty({required: false})
    name: string;

    @ApiModelProperty({required: false})
    description: string;

    @ApiModelProperty({required: false})
    createdAt: string;

    @ApiModelProperty({required: false})
    updateAt: string;

    public static from(dto: Partial<OuputAlbumDto>) {
        const it = new OuputAlbumDto();
        it.id = dto.id;
        it.name = dto.name;
        it.description = dto.description;
        it.createdAt = dto.createdAt;
        it.updateAt = dto.updateAt;
        return it;
    }

    public static fromEntity(entity: Album) {
        return this.from({
            id: Number(entity.id),
            name: entity.name,
            description: entity.description,
            updateAt: entity.lastChangedDateTime.toString(),
            createdAt: entity.createDateTime.toString()
        });
    }

    static AlbumDtoToEntity(photo: AlbumDto): Album {
        const it = new Album();
        it.name = photo.name;
        it.description = photo.description;
        return it;
    }


}
