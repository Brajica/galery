import {ApiProperty} from "@nestjs/swagger";
import {ApiResponseModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import {Photo} from "../model/photo.entity";
import {Cloudinary} from "../utils/cloudinary/cloudinary.service";
import {PhotoDTO} from "./photo.dto";
import {AlbumService} from "../modules/album/album.service";

export class OutputPhotoDto {


    @ApiResponseModelProperty()
    id: number;

    @ApiProperty()
    name: string;

    @ApiProperty()
    description: string;

    @ApiResponseModelProperty()
    url: string;

    @ApiResponseModelProperty()
    createdAt: string;

    @ApiResponseModelProperty()
    updatedAt: string;

    public static from(dto: Partial<OutputPhotoDto>) {
        const it = new OutputPhotoDto();
        it.id = dto.id;
        it.name = dto.name;
        it.url = dto.url;
        it.description = dto.description;
        it.createdAt = dto.createdAt;
        it.updatedAt = dto.updatedAt;
        return it;
    }

    public static fromEntity(entity: Photo) {
        return this.from({
            id: Number(entity.id),
            name: entity.name,
            url: entity.url,
            description: entity.description,
            updatedAt: entity.lastChangedDateTime.toString(),
            createdAt: entity.createDateTime.toString()
        });
    }

    static async ToEntity(photo: PhotoDTO): Promise<Photo> {
        const it = new Photo();
        it.name = photo.name;
        it.description = photo.description;
        it.url = await Cloudinary.base64_decode(photo.file);
        it.album = photo.album
        return it;
    }
}
