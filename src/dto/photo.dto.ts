import {ApiProperty} from "@nestjs/swagger";
import {Photo} from "../model/photo.entity";
import {Cloudinary} from "../utils/cloudinary/cloudinary.service";
import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import {Album} from "../model/album.entity";

export class PhotoDTO {

    id: any;

    @ApiProperty({required: true})
    name: string;

    @ApiProperty({required: true})
    description: string;

    @ApiProperty({required: true})
    file: string;

    @ApiModelProperty({required: false})
    albumId: number;


    album: Album;
}
