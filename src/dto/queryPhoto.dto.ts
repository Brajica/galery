import {ApiQuery} from "@nestjs/swagger";
import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";

export class QueryPhotoDto {
    @ApiModelProperty({required: false})
    name: string;

    @ApiModelProperty({required: false})
    albumId: number;

    @ApiModelProperty({required: false})
    date: Date;
}
