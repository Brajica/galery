import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {configService} from './config/config.service';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, {cors: true});
    if (!configService.isProduction()) {

        const document = SwaggerModule.createDocument(app, new DocumentBuilder()
            .setTitle('Api Galery Brayan')
            .setDescription('My Item API')
            .build());

        SwaggerModule.setup('api', app, document);

    }
    await app.listen(5000);
}

bootstrap();
