import {MigrationInterface, QueryRunner} from "typeorm";

export class myIinit1580142601226 implements MigrationInterface {
    name = 'myIinit1580142601226'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "photo" DROP CONSTRAINT "FK_464bcdec1590ef4d623166f1b54"`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" DROP COLUMN "albumId"`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD "albumId" integer`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" DROP COLUMN "createDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD "createDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ALTER COLUMN "createdBy" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" DROP COLUMN "lastChangedDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD "lastChangedDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ALTER COLUMN "lastChangedBy" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "album" DROP COLUMN "createDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ADD "createDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ALTER COLUMN "createdBy" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "album" DROP COLUMN "lastChangedDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ADD "lastChangedDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ALTER COLUMN "lastChangedBy" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" DROP COLUMN "createDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD "createDateTime" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" DROP COLUMN "lastChangedDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD "lastChangedDateTime" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ALTER COLUMN "description" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "album" DROP COLUMN "createDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ADD "createDateTime" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`, undefined);
        await queryRunner.query(`ALTER TABLE "album" DROP COLUMN "lastChangedDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ADD "lastChangedDateTime" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ALTER COLUMN "description" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD CONSTRAINT "FK_464bcdec1590ef4d623166f1b54" FOREIGN KEY ("albumId") REFERENCES "album"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "photo" DROP CONSTRAINT "FK_464bcdec1590ef4d623166f1b54"`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ALTER COLUMN "description" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "album" DROP COLUMN "lastChangedDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ADD "lastChangedDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP`, undefined);
        await queryRunner.query(`ALTER TABLE "album" DROP COLUMN "createDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ADD "createDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ALTER COLUMN "description" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" DROP COLUMN "lastChangedDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD "lastChangedDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" DROP COLUMN "createDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD "createDateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ALTER COLUMN "lastChangedBy" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "album" DROP COLUMN "lastChangedDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ADD "lastChangedDateTime" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ALTER COLUMN "createdBy" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "album" DROP COLUMN "createDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "album" ADD "createDateTime" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ALTER COLUMN "lastChangedBy" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" DROP COLUMN "lastChangedDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD "lastChangedDateTime" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ALTER COLUMN "createdBy" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" DROP COLUMN "createDateTime"`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD "createDateTime" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" DROP COLUMN "albumId"`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD "albumId" integer`, undefined);
        await queryRunner.query(`ALTER TABLE "photo" ADD CONSTRAINT "FK_464bcdec1590ef4d623166f1b54" FOREIGN KEY ("albumId") REFERENCES "album"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

}
