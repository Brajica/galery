import {Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany} from 'typeorm';
import {BaseEntity} from './base.entity';
import {Photo} from './photo.entity';

@Entity({name: 'album'})
export class Album extends BaseEntity {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column({type: 'varchar', length: 300})
    name: string;

    @Column({type: 'varchar', length: 300, nullable: true})
    description: string;

    @OneToMany(type => Photo, photo => photo.album)
    photos: Photo[];

    @Column({type: 'boolean', default: true})
    isActive: boolean;

    @Column({type: 'boolean', default: false})
    isArchived: boolean;

    @CreateDateColumn({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)'})
    createDateTime: Date;

    @Column({type: 'varchar', length: 300, nullable: true})
    createdBy: string;

    @UpdateDateColumn({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)'})
    lastChangedDateTime: Date;

    @Column({type: 'varchar', length: 300, nullable: true})
    lastChangedBy: string;

    @Column({type: 'varchar', length: 300, nullable: true})
    internalComment: string | null;
}
