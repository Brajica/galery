import {PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn} from 'typeorm';

export abstract class BaseEntity {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column({type: 'boolean', default: true})
    isActive: boolean;

    @Column({type: 'boolean', default: false})
    isArchived: boolean;

    @CreateDateColumn({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)'})
    createDateTime: Date;

    @Column({type: 'varchar', length: 300, nullable: true})
    createdBy: string;

    @UpdateDateColumn({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)'})
    lastChangedDateTime: Date;

    @Column({type: 'varchar', length: 300, nullable: true})
    lastChangedBy: string;

    @Column({type: 'varchar', length: 300, nullable: true})
    internalComment: string | null;
}
