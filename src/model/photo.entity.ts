import {
    Entity,
    Column,
    OneToOne,
    ManyToMany,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn, ManyToOne,
} from 'typeorm';
import {BaseEntity} from './base.entity';
import {Album} from './album.entity';

@Entity({name: 'photo'})
export class Photo extends BaseEntity {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column({type: 'varchar', length: 300})
    name: string;

    @Column({type: 'varchar', length: 300, nullable: true})
    description: string;

    @Column({type: 'varchar', length: 300})
    url: string;

    @ManyToOne(type => Album, album => album.photos)
    album: Album;

    @Column({type: 'boolean', default: true})
    isActive: boolean;

    @Column({type: 'boolean', default: false})
    isArchived: boolean;

    @CreateDateColumn({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)'})
    createDateTime: Date;

    @Column({type: 'varchar', length: 300, nullable: true})
    createdBy: string;

    @UpdateDateColumn({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)'})
    lastChangedDateTime: Date;

    @Column({type: 'varchar', length: 300, nullable: true})
    lastChangedBy: string;

    @Column({type: 'varchar', length: 300, nullable: true})
    internalComment: string | null;
}
