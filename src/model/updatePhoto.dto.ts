import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";

export class UpdatePhotoDto {
    @ApiModelProperty({required: false})
    name: string;

    @ApiModelProperty({required: false})
    description: string;

    @ApiModelProperty({required: false})
    albumId: number;
}
