import {Body, Controller, Get, Post, Query} from '@nestjs/common';
import {ApiCreatedResponse, ApiOkResponse} from "@nestjs/swagger";
import {OutputPhotoDto} from "../../dto/outputPhoto.dto";
import {AlbumService} from "./album.service";
import {QueryAlbumDto} from "../../dto/queryAlbum.dto";
import {OuputAlbumDto} from "../../dto/ouputAlbum.dto";
import {AlbumDto} from "../../dto/almbum.dto";

@Controller('album')
export class AlbumController {

    constructor(private readonly albumService: AlbumService) {
    }

    @Get()
    @ApiOkResponse({isArray: true})
    async getAll(@Query() query: QueryAlbumDto): Promise<OuputAlbumDto[]> {
        return await this.albumService.getAll(query);
    }

    @Post()
    @ApiCreatedResponse({type: OuputAlbumDto})
    async createAlbum(@Body() albumDto: AlbumDto): Promise<OuputAlbumDto> {
        return await this.albumService.create(albumDto);
    }
}
