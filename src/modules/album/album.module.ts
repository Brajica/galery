import {Module} from '@nestjs/common';
import {AlbumService} from './album.service';
import {AlbumController} from './album.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Album} from "../../model/album.entity";

@Module({
    imports: [
        TypeOrmModule.forFeature([Album])
    ],
    providers: [AlbumService],
    controllers: [AlbumController],
    exports: [AlbumService]
})
export class AlbumModule {
}
