import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {Album} from "../../model/album.entity";
import {OuputAlbumDto} from "../../dto/ouputAlbum.dto";
import {QueryAlbumDto} from "../../dto/queryAlbum.dto";
import {Photo} from "../../model/photo.entity";
import {OutputPhotoDto} from "../../dto/outputPhoto.dto";
import {AlbumDto} from "../../dto/almbum.dto";

@Injectable()
export class AlbumService {

    constructor(@InjectRepository(Album) private readonly repo: Repository<Album>) {
    }

    getAll(queryobj: QueryAlbumDto): Promise<OuputAlbumDto[]> {
        const query = this.repo.createQueryBuilder('photo')
            .where('photo.id IS NOT NULL')
        if (queryobj.name !== null && queryobj.name && queryobj.name !== '') {
            query.where(`photo.name LIKE '%${queryobj.name}%'`);
        }

        if (queryobj.date !== null && queryobj.date) {
            query.where(`photo."createDateTime"::date = :q`, {q: queryobj.date});
        }

        if (queryobj.description !== null && queryobj.description) {
            query.where(`photo."description" LIKE '%${queryobj.description}%'`);
        }


        return query.getMany().then((results: Album[]) => results.map(album => OuputAlbumDto.fromEntity(album)));
    }

    async create(album: AlbumDto): Promise<OuputAlbumDto> {
        return this.repo.save(OuputAlbumDto.AlbumDtoToEntity(album))
            .then(e => OuputAlbumDto.fromEntity(e));
    }

    async findById(id: number): Promise<OuputAlbumDto> {
        console.log(id)
        return this.repo.findOne(id)
            .then((e) => {
                return OuputAlbumDto.fromEntity(e)
            });
    }
}
