import {Body, Controller, Delete, Get, Param, Post, Put, Query, UploadedFiles, UseInterceptors} from '@nestjs/common';
import {ApiBody, ApiCreatedResponse, ApiOkResponse, ApiParam, ApiProperty, ApiResponse} from "@nestjs/swagger";
import {PhotoDTO} from "../../dto/photo.dto";
import {PhotoService} from "./photo.service";
import {OutputPhotoDto} from "../../dto/outputPhoto.dto";
import {QueryPhotoDto} from "../../dto/queryPhoto.dto";
import {UpdatePhotoDto} from "../../model/updatePhoto.dto";
import {AlbumDto} from "../../dto/almbum.dto";
import {AlbumService} from "../album/album.service";

@Controller('photo')
export class PhotoController {

    constructor(private readonly photoService: PhotoService,
                private readonly albumService: AlbumService) {
    }

    @Get()
    @ApiOkResponse({isArray: true})
    async getAll(@Query() query: QueryPhotoDto): Promise<OutputPhotoDto[]> {
        return await this.photoService.getAll(query)
    }

    @Post('')
    @ApiCreatedResponse({type: OutputPhotoDto})
    async createPhoto(@Body() photo: PhotoDTO): Promise<OutputPhotoDto> {
        if (photo.albumId) {
            photo.album = AlbumDto.OutputtoEntity(await this.albumService.findById(photo.albumId));
        }

        return await this.photoService.create(photo);
    }

    @Put('/:id')
    @ApiResponse({type: OutputPhotoDto})
    async update(@Body() photo: UpdatePhotoDto, @Param('id') id: number): Promise<OutputPhotoDto> {
        return await this.photoService.update(photo, id);
    }

    @Delete('/:id')
    @ApiOkResponse()
    async deletePhoto(@Param('id') id: number): Promise<any> {
        return await this.photoService.delete(id)
    }


    @Delete('delete-album/:id')
    @ApiResponse({type: OutputPhotoDto})
    async deletePhotoAlbum(@Param('id') id: number): Promise<OutputPhotoDto> {
        return await this.photoService.deletePhotoAlbum(id);
    }


}
