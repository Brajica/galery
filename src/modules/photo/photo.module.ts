import {Module} from '@nestjs/common';
import {PhotoController} from './photo.controller';
import {PhotoService} from './photo.service';
import {Photo} from "../../model/photo.entity";
import {TypeOrmModule} from "@nestjs/typeorm";
import {AlbumModule} from "../album/album.module";


@Module({
    imports: [
        TypeOrmModule.forFeature([Photo]),
        AlbumModule
    ],
    controllers: [PhotoController],
    providers: [PhotoService],
})
export class PhotoModule {
}
