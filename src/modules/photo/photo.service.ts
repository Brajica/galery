import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Photo} from '../../model/photo.entity';
import {Repository} from 'typeorm';
import {PhotoDTO} from '../../dto/photo.dto';
import {OutputPhotoDto} from "../../dto/outputPhoto.dto";
import {QueryPhotoDto} from "../../dto/queryPhoto.dto";
import {UpdatePhotoDto} from "../../model/updatePhoto.dto";
import {AlbumService} from "../album/album.service";
import {AlbumDto} from "../../dto/almbum.dto";
import {Album} from "../../model/album.entity";


@Injectable()
export class PhotoService {
    constructor(@InjectRepository(Photo) private readonly repo: Repository<Photo>,
                private albumService: AlbumService) {
    }


    public async getAll(queryobj: QueryPhotoDto): Promise<OutputPhotoDto[]> {


        const query = this.repo.createQueryBuilder('photo')
            .where('photo.id IS NOT NULL')
        if (queryobj.name !== null && queryobj.name && queryobj.name !== '') {
            query.andWhere(`photo.name LIKE '%${queryobj.name}%'`);
        }

        if (queryobj.date !== null && queryobj.date) {
            query.andWhere(`photo."createDateTime"::date = :q::date`, {q: queryobj.date});
        }

        if (queryobj.albumId !== null && queryobj.albumId) {
            query.andWhere(`photo."albumId" = ${Number(queryobj.albumId)}`);
        }


        return query.getMany().then((results: Photo[]) => results.map(photo => OutputPhotoDto.fromEntity(photo)));

    }

    public async create(photo: PhotoDTO): Promise<OutputPhotoDto> {
        return this.repo.save(await OutputPhotoDto.ToEntity(photo))
            .then(e => OutputPhotoDto.fromEntity(e));
    }

    public async update(photo: UpdatePhotoDto, id: number): Promise<OutputPhotoDto> {
        const insert = this.repo.createQueryBuilder()
            .update(Photo);

        if (photo.description !== null && photo.description !== '' && photo.description) {
            insert.set({description: photo.description});
        }

        if (photo.name !== null && photo.name !== '' && photo.name) {
            insert.set({name: photo.name});
        }

        if (photo.albumId !== null && photo.albumId) {
            insert.set(
                {
                    album: AlbumDto.OutputtoEntity(await this.albumService.findById(photo.albumId)),
                },
            );
        }

        insert.where('id = :Id', {Id: id})

        return insert.execute().then(e => this.findById(id));
    }

    public async delete(id: number): Promise<any> {
        return this.repo.createQueryBuilder()
            .delete().from('photo')
            .where('id = :Id', {Id: id})
            .execute();
    }

    async findById(id: number): Promise<OutputPhotoDto> {
        return this.repo.findOne(id)
            .then((e) => OutputPhotoDto.fromEntity(e))
    }

    async deletePhotoAlbum(id: number): Promise<OutputPhotoDto> {
        const update = this.repo.createQueryBuilder()
            .update(Photo)
            .set({
                album: new Album(),
            })

        update.where('id = :Id', {Id: id})
        return update.execute().then(e => this.findById(id));
    }
}
