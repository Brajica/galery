import ErrnoException = NodeJS.ErrnoException;

const fs = require('fs');
// tslint:disable-next-line:no-var-requires
const base64ToImage = require('base64-to-image');
require('dotenv').config();
// tslint:disable-next-line:no-var-requires
const cloudinary = require('cloudinary');
cloudinary.config(
    {
        cloud_name: 'dqe1i1omv',
        api_key: '279614486678948',
        api_secret: '--4R3sWCJRj4B41bwszLt1rUsF8',
    },
)

export class Cloudinary {

    static uploadFileToCloudinary(file): Promise<string> {
        return new Promise(resolve => {
            cloudinary.uploader.upload(__dirname + '/' + file, (result, error) => {
                fs.unlink(__dirname + '/' + file, (e: ErrnoException, re) => {
                    if (!e) {
                        resolve(result.url.toString());
                    }
                });
            });
        });
    }

    static async base64_decode(base64str): Promise<string> {
        // create buffer object from base64 encoded string, it is important to tell the constructor that the string is base64 encoded
        const base64Str = base64str;
        const path = __dirname + '/';

        const s = await base64ToImage(base64Str, path);
        return await this.uploadFileToCloudinary(s.fileName);
    }
}
